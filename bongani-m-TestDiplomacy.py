#!/usr/bin/env python3

# -------------------------------
# Copyright (C) 2019
# Helen Salgi & Bongani Mbigi
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_print, diplomacy_solve, diplomacy_state, remove_support, diplomacy_eval, diplomacy_result

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # -----------
    # Tests diplomacy_solve
    # -----------
    def test_solve1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    # -----------
    # Tests diplomacy_state
    # -----------
    def test_state1(self):
        input_data = ["A", "Madrid", "Hold"]
        expected = {'A': {'city': 'Madrid', 'support': [], 'attackers': []}}
        test_state = {}
        diplomacy_state(test_state, input_data)
        self.assertEqual(test_state, expected)

    def test_state2(self):
        input_data = ["A", "Madrid", "Hold"]
        expected = {'A': {'city': 'Madrid', 'support': [], 'attackers': [
            'B']}, 'B': {'city': 'Madrid', 'support': [], 'attackers': []}}
        test_state = {}
        diplomacy_state(test_state, input_data)
        input_data = ["B", "Barcelona", "Move", "Madrid"]
        diplomacy_state(test_state, input_data)
        self.assertEqual(test_state, expected)

    def test_state3(self):
        expected = {'A': {'city': 'Madrid', 'support': [], 'attackers': ['B']}, 'B': {
            'city': 'Madrid', 'support': ['C'], 'attackers': []}, 'C': {'city': 'London', 'support': [], 'attackers': []}}

        event_1 = ["A", "Madrid", "Hold"]
        event_2 = ["B", "Barcelona", "Move", "Madrid"]
        event_3 = ["C", "London", "Support", "B"]

        test_state = {}

        diplomacy_state(test_state, event_1)
        diplomacy_state(test_state, event_2)
        diplomacy_state(test_state, event_3)
        self.assertEqual(test_state, expected)

    # -----------
    # Tests diplomacy_eval
    # -----------
    def test_eval(self):
        test_state = {'A': {'city': 'Madrid', 'support': [], 'attackers': ['B']}, 'B': {
            'city': 'Barcelona', 'support': [], 'attackers': []}}
        expected = {'A': {'city': '[dead]', 'support': [], 'attackers': ['B']}, 'B': {
            'city': '[dead]', 'support': [], 'attackers': []}}
        diplomacy_eval(test_state)
        self.assertEqual(test_state, expected)

    # -----------
    # Tests diplomacy_result
    # -----------
    def test_result(self):
        test_state = {'A': {'city': 'Madrid', 'support': [], 'attackers': []}, 'B': {
            'city': 'Barcelona', 'support': [], 'attackers': []}}

        result = diplomacy_result(test_state)
        self.assertEqual(result, ['A Madrid\n', 'B Barcelona\n'])

    # -----------
    # Tests remove_support
    # -----------
    def test_remove_support(self):
        test_state = {'A': {'city': 'Madrid', 'support': [], 'attackers': ['B']}, 'B': {
            'city': 'Barcelona', 'support': ['C'], 'attackers': []}, 'C': {'city': 'London', 'support': [], 'attackers': []}}
        expected_state = {'A': {'city': 'Madrid', 'support': [], 'attackers': ['B']}, 'B': {
            'city': 'Barcelona', 'support': [], 'attackers': []}, 'C': {'city': 'London', 'support': [], 'attackers': []}}
        remove_support(test_state, 'C')
        self.assertEqual(test_state, expected_state)

    def test_remove_support_2(self):
        test_state = {'A': {'city': 'Madrid', 'support': [], 'attackers': ['B']}, 'B': {
            'city': 'Barcelona', 'support': ['C'], 'attackers': []}, 'C': {'city': 'London', 'support': [], 'attackers': []}}
        expected_state = {'A': {'city': 'Madrid', 'support': [], 'attackers': ['B']}, 'B': {
            'city': 'Barcelona', 'support': ['C'], 'attackers': []}, 'C': {'city': 'London', 'support': [], 'attackers': []}}
        remove_support(test_state, 'D')
        self.assertEqual(test_state, expected_state)

    # -----------
    # Tests diplomacy_print
    # -----------
    def test_print(self):
        w = StringIO()
        diplomacy_print(['Madrid\n'], w)
        self.assertEqual(w.getvalue(), 'Madrid\n')


# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestDiplomacy.out



% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
